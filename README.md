# Énumération

> Quelques problèmes de dénombrement, dont l'énoncé est simple, mais dont la résolution peut faire intervenir des méthodes variées en mathématiques et en algorithmique.

- Plusieurs méthodes sont proposées, avec toujours un code en *Python*. Les bonnes pratiques de programmation sont mises en avant, avec, en particulier, une première approche par force brute.
- Les méthodes avancées nécessitent des connaissances en algèbre linéaire ou en arithmétique, et sont destinées au public d'enseignants de mathématiques, ou d'étudiants.

## Nombre de triangles dans un triangle

> Exemple de méthodes avec des polynômes, des coefficients binomiaux, ...

## Nombre de pavages d'un rectangle 4×n par des rectangles d'aire 4

> Exemple de méthodes sur les suites récurrentes linéaires

## Nombre de constructions de murs en brique sans fissure

> Exemple de méthodes sur les graphes creux

## Dénombrer les nombres premiers

> Exemple de méthodes en arithmétique

## Des carnets Jupyter

> Exemples variés de méthodes en probabilités, arithmétique, géométrie, suites...

Ces carnets (mathématiques) sont issus d'exemples d'une formation Python pour les enseignants.
